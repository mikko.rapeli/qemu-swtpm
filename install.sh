#!/bin/sh

set -eu

PREFIX=${PREFIX:-/usr/local}
DESTDIR=${DESTDIR:=}
BINARIES=$(find /usr/bin /usr/local/bin -name qemu-system-\* -not -name \*-swtpm | xargs -n 1 basename)

install -v -d ${DESTDIR}${PREFIX}/bin
install -v -m 0755 qemu-swtpm ${DESTDIR}${PREFIX}/bin
for qemu in ${BINARIES}; do \
	ln -sfv qemu-swtpm ${DESTDIR}${PREFIX}/bin/${qemu}-swtpm; \
done

if [ -f qemu-swtpm.1 ]; then
	install -v -d ${DESTDIR}${PREFIX}/share/man/man1
	install -v -m 0644 qemu-swtpm.1 ${DESTDIR}${PREFIX}/share/man/man1
	for qemu in ${BINARIES}; do ln -sfv qemu-swtpm.1 ${DESTDIR}${PREFIX}/share/man/man1/${qemu}-swtpm.1; done
fi
