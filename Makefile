all: qemu-swtpm.1

qemu-swtpm.1: qemu-swtpm.pod
	pod2man --verbose --name qemu-swtpm -c '' -r '' --utf8 $< $@ || ($(RM) $@; false)

install:
	./install.sh

docker-image:
	docker build -t linaro/ci-amd64-ledge:swtpm .

clean:
	$(RM) qemu-swtpm.1

distclean:
	@echo Nothing to do
