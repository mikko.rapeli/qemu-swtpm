#!/bin/sh

set -xeu

ARCH=aarch64
IMAGES_DIR=tmp
DOCKER_IMAGE_NAME=qemu-swtpm-test-image
DOCKER_CONTAINER_NAME=qemu-swtpm-test-container
BOOT_CHECK_STRING="TRS (Trusted Reference Stack) unstable trs-qemuarm64"

mkdir -p ${IMAGES_DIR}

OS=trs-image-trs-qemuarm64.rootfs.wic
FIRMWARE=flash.bin-qemu

OS_URL="https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/${OS}.gz?job=build-meta-trs"
FIRMWARE_URL="https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/${FIRMWARE}.gz?job=build-meta-ts-qemuarm64-secureboot"

if [ ! -f ${IMAGES_DIR}/${OS} ]
then
	wget ${OS_URL} -O - | gunzip > ${IMAGES_DIR}/${OS}
fi

if [ ! -f ${IMAGES_DIR}/${FIRMWARE} ]
then
	wget ${FIRMWARE_URL} -O - | gunzip > ${IMAGES_DIR}/${FIRMWARE}
fi

docker build -t ${DOCKER_IMAGE_NAME} .
(docker \
	run \
	--network=host \
	--cap-add=NET_ADMIN \
	--interactive \
	--rm \
	--init \
	--name=${DOCKER_CONTAINER_NAME} \
	--mount=type=bind,source=${PWD}/${IMAGES_DIR},destination=/tmp/images \
	${DOCKER_IMAGE_NAME}:latest \
		/usr/local/bin/qemu-system-${ARCH}-swtpm \
		-cpu cortex-a57 \
		-machine virt,secure=on \
		-nographic \
		-net nic,model=virtio,macaddr=DE:AD:BE:EF:36:02 \
		-net user \
		-m 1024 \
		-monitor none \
		-drive id=disk1,file=/tmp/images/${OS},if=none,format=raw \
		-device virtio-blk-device,drive=disk1 \
		-nographic \
		-device i6300esb,id=watchdog0 \
		-m 2048 \
		-smp 4 \
		-drive if=pflash,unit=0,readonly=off,file=/tmp/images/${FIRMWARE},format=raw
) 2>&1 > ${IMAGES_DIR}/log & 

# Wait for log file to be created
while true; do
	if test -f ${IMAGES_DIR}/log; then
		break
	fi
	sleep 1
done

# Wait until it boots
echo "Waiting up to 10 min to boot"
timeout 10m tail -f ${IMAGES_DIR}/log | sed "/${BOOT_CHECK_STRING}/ q"
RC=$?
docker stop ${DOCKER_CONTAINER_NAME}
exit ${RC}
