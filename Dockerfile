FROM debian:bookworm
RUN apt-get update -q && apt-get install -qy qemu-system qemu-system-gui- swtpm swtpm-tools
COPY install.sh /build/
COPY qemu-swtpm /build/
RUN cd /build/ && ./install.sh && cd - && rm -rf /build/
