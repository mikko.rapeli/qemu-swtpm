=head1 NAME

qemu-swtpm - QEMU wrapper for starting VMs with an attached TPM emulator

=head1 SYNOPSIS

B<qemu-system-*-swtpm> [I<OPTIONS>]

=head1 DESCRIPTION

qemu-swtpm provides wrapper binaries called qemu-system-*-swtpm that will
start a swtpm instance and call the corresponding qemu-system-* binary with all
your arguments plus the right ones to make the emulated TPM available to the
virtual machine.

The QEMU binary to call is determined by dropping the -swtpm suffix from
the original invocation. All options are passed along to the QEMU
binary, and the swtpm-related options are appended to the end of the
command line options.

The TPM state will be kept across invocations, as long as the exact same
command line options are used, i.e., if you change the command line
options, qemu-swtpm assumes that to be a different device, and therefore
it has a different TPM.

=head1 SEE ALSO

L<B<qemu-system>>(1)

